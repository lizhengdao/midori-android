[![CircleCI](https://circleci.com/gh/midori-browser/core.svg?style=svg)](https://circleci.com/gh/midori-browser/core)
[![Telegram](https://img.shields.io/badge/Telegram-Chat-gray.svg?style=flat&logo=telegram&colorA=5583a4&logoColor=fff)](https://www.midori-browser.org/telegram)
[![Twitter](https://img.shields.io/twitter/follow/midoriweb.svg?style=social&label=Follow)](https://twitter.com/midoriweb)
[![Donate](https://img.shields.io/badge/PayPal-Donate-gray.svg?style=flat&logo=paypal&colorA=0071bb&logoColor=fff)](https://www.midori-browser.org/donate)
[![BountySource](https://img.shields.io/bountysource/team/midori/activity.svg)](https://www.bountysource.com/teams/midori)
[![Patreon](https://img.shields.io/badge/PATREON-Pledge-red.svg)](https://www.patreon.com/midoribrowser)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/589532a3aca949ad86b092ed54461c54)](https://www.codacy.com/gl/midori-web/midori-android?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=midori-web/midori-android&amp;utm_campaign=Badge_Grade)

<p align="center">
    <b>Midori</b>
    a lightweight, fast and free web browser for Android
</p>

![Midori Screenshot](https://www.astian.org/wp-content/uploads/2019/05/photo_2019-05-06_13-07-57-e1557167694150.jpg)

Midori is a lightweight yet powerful web browser which runs just as well on little embedded computers named for delicious pastries as it does on beefy machines with a core temperature exceeding that of planet earth. And it looks good doing that, too. Oh, and of course it's free software. And for Android

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/org.midorinext.android/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="80">](https://play.google.com/store/apps/details?id=org.midorinext.android)

**Privacy out of the box**

* Adblock filter list support.
* Private browsing.
* Manage cookies and scripts.

**Productivity features**

* Opens many Tabs.
* History Manage
* Download Manage.
* Bookmark.
* Dark Theme.
* Share Content.
* and much more.

![Midori Screenshot](https://www.astian.org/wp-content/uploads/2019/05/photo_2019-05-06_13-30-41-2-e1557167891746.jpg)
