package org.midorinext.android.browser

import org.midorinext.android.preference.IntEnum

enum class CustomBar(override val value: Int) : IntEnum {
    NONE(0),
    COLOR(1)
}

