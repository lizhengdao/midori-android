package org.midorinext.android.html.homepage

import android.app.Application
import dagger.Reusable
import io.reactivex.Single
import io.reactivex.rxkotlin.subscribeBy
import org.json.JSONObject
import org.midorinext.android.AppTheme
import org.midorinext.android.R
import org.midorinext.android.constant.FILE
import org.midorinext.android.constant.UTF8
import org.midorinext.android.html.HtmlPageFactory
import org.midorinext.android.html.jsoup.*
import org.midorinext.android.preference.UserPreferences
import org.midorinext.android.search.SearchEngineProvider
import java.io.BufferedReader
import java.io.File
import java.io.FileWriter
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import javax.inject.Inject

/**
 * A factory for the home page.
 */
@Reusable
class HomePageFactory @Inject constructor(
        private val application: Application,
        private val searchEngineProvider: SearchEngineProvider,
        private val homePageReader: HomePageReader,
        private var userPreferences: UserPreferences
) : HtmlPageFactory {

    private val title = application.getString(R.string.home)

    override fun buildPage(): Single<String> = Single
            .just(searchEngineProvider.provideSearchEngine())
            .map { (iconUrl, queryUrl, _) ->
                parse(homePageReader.provideHtml()) andBuild {
                    title { title }
                    charset { UTF8 }
                    body {
                        //document.getElementById('Id').value='new value';
                        id("search_input") { attr("style", "background: url('" + iconUrl + "') no-repeat scroll 7px 7px;background-size: 22px 22px;") }
                    }
                }
            }
            .map { content -> Pair(createHomePage(), content) }
            .doOnSuccess { (page, content) ->
                FileWriter(page, false).use {
                    var html = sendGet();
                    val jsonObj2 = JSONObject(html.substring(html.indexOf("{"), html.lastIndexOf("}") + 1))
                    val foodJson = jsonObj2.getJSONArray("data")
                    var data = ""
                    var data2 = ""
                    var contador = 0
                    val url2 = ""
                    for (i in 0..foodJson!!.length() - 1) {
                        //val categories = FoodCategoryObject()
                        val name = foodJson.getJSONObject(i).getString("brand")
                        val imagen = foodJson.getJSONObject(i).getString("iurl")
                        val url = foodJson.getJSONObject(i).getString("rurl")
                        val imagenpixel = foodJson.getJSONObject(i).getString("impurl")
                        println("Esta es la url: $url")
                        data += "<div onclick=\"window.location.href = '$url';\"  style='float:left;margin-right:15px'>"+ "<img style='height:1px;width:1px;display:none' src ='$imagenpixel'>" + "<img  src ='$imagen' " +
                                "style ='-webkit-box-shadow: 0px 0px 10px 0px #A4A4A4;\n" + "-moz-box-shadow: 0px 0px 10px 0px #12a90b;\n" + "width:100%;\n" +
                                "box-shadow: 0px 0px 10px 0px #A4A4A4;height:75px;width:75px;border-radius:5%'>"+ "<p style='font-size:12px'>"+ name +"</p>"+"</br>"+"</div>"
                    }
                    if(userPreferences.startPageThemeEnabled && userPreferences.useTheme == AppTheme.LIGHT){
                        it.write("<div style = 'margin-top:10px;margin-left:30px;margin-bottom:30px;'>$data</div>$content")
                    }
                    else if(userPreferences.startPageThemeEnabled && userPreferences.useTheme == AppTheme.BLACK){
                        it.write("<div style = 'margin-top:30px;margin-left:30px;margin-bottom:60px;'>"+data+"</div>" + content + "<style>body {\n" +
                                "    background-color: #ffffff;\n" +
                                "}</style>")
                    }
                    else if(userPreferences.startPageThemeEnabled && userPreferences.useTheme == AppTheme.DARK){
                        it.write("<div style = 'margin-top:30px;margin-left:30px;margin-bottom:60px;'>"+data+"</div>" + content + "<style>body {\n" +
                                "    background-color: #ffffff;\n" +
                                "}</style>")
                    }
                    else{
                        it.write("<div style = 'margin-top:20px;margin-left:30px;margin-bottom:60px;'>$data</div>$content")
                    }

                }
            }
            .map { (page, _) -> "$FILE$page" }
    /**
     * Create the home page file.
     */

    fun createHomePage() = File(application.filesDir, FILENAME)

    fun sendGet(): StringBuffer {

        val url = "http://unh85.veve.com/qlapi?o=unh85&s=92433&is=96x96&n=8"
        val obj = URL(url)

        with(obj.openConnection() as HttpURLConnection) {
            // optional default is GET
            requestMethod = "GET"

            BufferedReader(InputStreamReader(inputStream)).use {
                val response = StringBuffer()
                var inputLine = it.readLine()
                while (inputLine != null) {
                    response.append(inputLine)
                    inputLine = it.readLine()
                }
                /*println(response.toString())*/
                return response;
            }
        }
    }

    companion object {

        const val FILENAME = "homepage.html"

    }

}

private fun Any.getJSONObject(i: Any) {

}
