/*
 * Copyright 2014 A.C.R. Development
 */
package org.midorinext.android.settings.fragment

import org.midorinext.android.BuildConfig
import org.midorinext.android.R
import android.os.Bundle
import android.preference.Preference
import android.text.Html
import androidx.appcompat.app.AlertDialog

class AboutSettingsFragment : AbstractSettingsFragment() {

    override fun providePreferencesXmlResource() = R.xml.preference_about

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        clickablePreference(
            preference = SETTINGS_VERSION,
            summary = "v" + BuildConfig.VERSION_NAME,
            onClick = { }
        )

        var pref: Preference = findPreference(SETTINGS_VERSION)
        pref.setOnPreferenceClickListener {
            val builder = AlertDialog.Builder(activity)
            builder.setTitle("Midori Next Browser Version " + BuildConfig.VERSION_NAME)
            builder.setMessage("What's new:\nMidori Next is a lightweight,\n fast, free & open-source web browser")


            builder.setPositiveButton(resources.getString(R.string.action_ok)){dialogInterface , which ->

            }
            val alertDialog: AlertDialog = builder.create()
            alertDialog.setCancelable(false)
            alertDialog.show()
            true
        }
    }

    companion object {
        private const val SETTINGS_VERSION = "pref_version"
    }
}
